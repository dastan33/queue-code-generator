create table public.queue_code
(
    queue_code_id bigserial
        primary key,
    code          text,
    created_time  timestamp
);
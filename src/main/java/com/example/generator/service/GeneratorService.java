package com.example.generator.service;

import com.example.generator.entity.QueueCode;
import com.example.generator.repo.QueueCodeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class GeneratorService {

    private final QueueCodeRepo queueCodeRepo;

    private final String lowercaseChars = "abcdefghijklmnopqrstuvwxyz";

    private final String digits = "0123456789";

    @Autowired
    public GeneratorService(
            QueueCodeRepo queueCodeRepo
    ) {
        this.queueCodeRepo = queueCodeRepo;
    }

    public String getNextCode() {
        QueueCode queueCode = this.queueCodeRepo.getLastCode();
        String nextCodeStr = this.generateNextCode(queueCode.getCode());
        QueueCode nextQueueCode = new QueueCode();
        nextQueueCode.setCode(nextCodeStr);
        nextQueueCode.setCreatedTime(new Date());
        this.queueCodeRepo.save(nextQueueCode);
        return nextCodeStr;
    }

    public String generateNextCode(String code) {
        if (code == null || code.isEmpty()) {
            throw new IllegalArgumentException("Code must not be null or empty");
        }
        if (code.length() < 4 || code.length() % 2 == 1) {
            throw new IllegalArgumentException("Incorrect code");
        }
        for (int i = 0; i < code.length(); i++) {
            if ((i % 2 == 0 && !Character.isLowerCase(code.charAt(i)))
                    || (i % 2 == 1 && !Character.isDigit(code.charAt(i)))) {
                throw new IllegalArgumentException("Incorrect code");
            }
        }
        int lastIncrementableCharIndex = -1;
        for (int i = code.length() - 1; i >= 0; i--) {
            if ((i % 2 == 0 && code.charAt(i) != 'z') || (i % 2 == 1 && code.charAt(i) != '9')) {
                lastIncrementableCharIndex = i;
                break;
            }
        }
        if (lastIncrementableCharIndex != -1) {
            StringBuilder newCode = new StringBuilder();
            for (int i = 0; i < code.length(); i++) {
                if (i < lastIncrementableCharIndex) {
                    newCode.append(code.charAt(i));
                }
                else if (i == lastIncrementableCharIndex) {
                    newCode.append(i % 2 == 0
                            ? this.getNextLowercase(code.charAt(i))
                            : this.getNextDigit(code.charAt(i)));
                }
                else {
                    newCode.append(i % 2 == 0 ? 'a' : '0');
                }
            }
            return newCode.toString();
        }
        else {
            return "a0".repeat(code.length() / 2 + 1);
        }
    }

    private char getNextLowercase(char c) {
        return lowercaseChars.charAt(lowercaseChars.indexOf(c) + 1);
    }

    private char getNextDigit(char c) {
        return digits.charAt(digits.indexOf(c) + 1);
    }

}

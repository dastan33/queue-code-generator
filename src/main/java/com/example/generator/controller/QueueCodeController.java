package com.example.generator.controller;

import com.example.generator.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/queue-codes")
public class QueueCodeController {

    private final GeneratorService generatorService;

    @Autowired
    public QueueCodeController(
            GeneratorService generatorService
    ) {
        this.generatorService = generatorService;
    }

    @PostMapping
    public String getNewCode() {
        return this.generatorService.getNextCode();
    }

}

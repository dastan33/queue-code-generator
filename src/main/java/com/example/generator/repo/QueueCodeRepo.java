package com.example.generator.repo;

import com.example.generator.entity.QueueCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface QueueCodeRepo extends JpaRepository<QueueCode, Long> {

    @Query(value = """
        SELECT *
        FROM queue_code
        ORDER BY created_time DESC
        LIMIT 1
    """, nativeQuery = true)
    QueueCode getLastCode();

}

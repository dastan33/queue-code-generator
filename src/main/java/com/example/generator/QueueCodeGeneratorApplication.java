package com.example.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QueueCodeGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(QueueCodeGeneratorApplication.class, args);
    }

}

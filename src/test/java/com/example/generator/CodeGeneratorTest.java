package com.example.generator;

import com.example.generator.service.GeneratorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@SpringBootTest
public class CodeGeneratorTest {

    @Autowired
    private GeneratorService generatorService;

    @TestFactory
    public Stream<DynamicTest> testIncorrectValues() {
        List<String> incorrectCodes = Arrays.asList("", null, "1", "asd2fae", "z3ff", "b4", "c3c", "4f5g1s", "a3b4c5e");
        return incorrectCodes.stream().map(code ->
                DynamicTest.dynamicTest("Testing code " + code, () ->
                        Assertions.assertThrows(IllegalArgumentException.class, () ->
                                this.generatorService.generateNextCode(code)
                        )
                )
        );
    }

    @TestFactory
    public Stream<DynamicTest> testCodes() {
        List<String> codesList = Arrays.asList("a0a0", "b0a5", "z9b4", "a0a9", "g7z9", "b9z9", "z9z9c8z9", "z9z9z9");
        List<String> nextCodes = Arrays.asList("a0a1", "b0a6", "z9b5", "a0b0", "g8a0", "c0a0", "z9z9c9a0", "a0a0a0a0");
        return codesList.stream().map(code ->
                DynamicTest.dynamicTest("Testing code " + code, () ->
                        Assertions.assertEquals(
                                this.generatorService.generateNextCode(code),
                                nextCodes.get(codesList.indexOf(code))
                        )
                )
        );
    }

}
